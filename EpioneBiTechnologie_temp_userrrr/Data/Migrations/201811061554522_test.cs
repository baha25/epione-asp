namespace Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        cin = c.Int(nullable: false),
                        firstName = c.String(nullable: false),
                        lastName = c.String(nullable: false),
                        gender = c.String(nullable: false),
                        Address = c.String(nullable: false),
                        Email = c.String(),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.CustomUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.CustomLogins",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LoginProvider = c.String(),
                        ProviderKey = c.String(),
                        UserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.CustomUserRoles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                        RoleId = c.Int(nullable: false),
                        CustomRole_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CustomRoles", t => t.CustomRole_Id)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.CustomRole_Id);
            
            CreateTable(
                "dbo.Appointments",
                c => new
                    {
                        appointmentId = c.Int(nullable: false, identity: true),
                        dateAppointment = c.DateTime(nullable: false),
                        reason = c.String(nullable: false),
                        state = c.Int(nullable: false),
                        message = c.String(),
                        price = c.Single(nullable: false),
                        Doctor_Id = c.Int(),
                        Patient_Id = c.Int(),
                        path_pathId = c.Int(),
                    })
                .PrimaryKey(t => t.appointmentId)
                .ForeignKey("dbo.Users", t => t.Doctor_Id)
                .ForeignKey("dbo.Users", t => t.Patient_Id)
                .ForeignKey("dbo.Paths", t => t.path_pathId)
                .Index(t => t.Doctor_Id)
                .Index(t => t.Patient_Id)
                .Index(t => t.path_pathId);
            
            CreateTable(
                "dbo.Chats",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        sender = c.String(nullable: false),
                        receiver = c.String(nullable: false),
                        objectC = c.String(nullable: false),
                        Doctor_Id = c.Int(),
                        Patient_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.Doctor_Id)
                .ForeignKey("dbo.Users", t => t.Patient_Id)
                .Index(t => t.Doctor_Id)
                .Index(t => t.Patient_Id);
            
            CreateTable(
                "dbo.Availabilities",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        startHour = c.Int(nullable: false),
                        endHour = c.Int(nullable: false),
                        Day = c.Int(nullable: false),
                        doctor_Id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Users", t => t.doctor_Id)
                .Index(t => t.doctor_Id);
            
            CreateTable(
                "dbo.Interventions",
                c => new
                    {
                        interventionId = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false),
                        doctor_Id = c.Int(),
                    })
                .PrimaryKey(t => t.interventionId)
                .ForeignKey("dbo.Users", t => t.doctor_Id)
                .Index(t => t.doctor_Id);
            
            CreateTable(
                "dbo.Paths",
                c => new
                    {
                        pathId = c.Int(nullable: false, identity: true),
                        comment = c.String(),
                    })
                .PrimaryKey(t => t.pathId);
            
            CreateTable(
                "dbo.Raitings",
                c => new
                    {
                        id = c.Int(nullable: false),
                        rating = c.Single(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Appointments", t => t.id)
                .Index(t => t.id);
            
            CreateTable(
                "dbo.CustomRoles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CustomUserRoles", "UserId", "dbo.Users");
            DropForeignKey("dbo.CustomLogins", "UserId", "dbo.Users");
            DropForeignKey("dbo.CustomUserClaims", "UserId", "dbo.Users");
            DropForeignKey("dbo.CustomUserRoles", "CustomRole_Id", "dbo.CustomRoles");
            DropForeignKey("dbo.Raitings", "id", "dbo.Appointments");
            DropForeignKey("dbo.Appointments", "path_pathId", "dbo.Paths");
            DropForeignKey("dbo.Interventions", "doctor_Id", "dbo.Users");
            DropForeignKey("dbo.Availabilities", "doctor_Id", "dbo.Users");
            DropForeignKey("dbo.Chats", "Patient_Id", "dbo.Users");
            DropForeignKey("dbo.Appointments", "Patient_Id", "dbo.Users");
            DropForeignKey("dbo.Chats", "Doctor_Id", "dbo.Users");
            DropForeignKey("dbo.Appointments", "Doctor_Id", "dbo.Users");
            DropIndex("dbo.Raitings", new[] { "id" });
            DropIndex("dbo.Interventions", new[] { "doctor_Id" });
            DropIndex("dbo.Availabilities", new[] { "doctor_Id" });
            DropIndex("dbo.Chats", new[] { "Patient_Id" });
            DropIndex("dbo.Chats", new[] { "Doctor_Id" });
            DropIndex("dbo.Appointments", new[] { "path_pathId" });
            DropIndex("dbo.Appointments", new[] { "Patient_Id" });
            DropIndex("dbo.Appointments", new[] { "Doctor_Id" });
            DropIndex("dbo.CustomUserRoles", new[] { "CustomRole_Id" });
            DropIndex("dbo.CustomUserRoles", new[] { "UserId" });
            DropIndex("dbo.CustomLogins", new[] { "UserId" });
            DropIndex("dbo.CustomUserClaims", new[] { "UserId" });
            DropTable("dbo.CustomRoles");
            DropTable("dbo.Raitings");
            DropTable("dbo.Paths");
            DropTable("dbo.Interventions");
            DropTable("dbo.Availabilities");
            DropTable("dbo.Chats");
            DropTable("dbo.Appointments");
            DropTable("dbo.CustomUserRoles");
            DropTable("dbo.CustomLogins");
            DropTable("dbo.CustomUserClaims");
            DropTable("dbo.Users");
        }
    }
}
