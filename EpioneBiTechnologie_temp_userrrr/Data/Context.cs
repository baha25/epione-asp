﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SqlServer;
namespace Data
{
    public class Context : DbContext
    {
        public Context() : base("name=amal")
        {

        }
        //les dbsets

        
        public DbSet<Admin> Admins{ get; set; }
      //  public DbSet<Address> Adress { get; set; }
    public DbSet<Appointment> Appointments { get; set; }
        public DbSet<Availability> Availabilitys { get; set; }
        public DbSet<Chat> Chats { get; set; }
        public DbSet<Doctor> Doctors { get; set; }
        public DbSet<Intervention> Interventions { get; set; }
        public DbSet<Path> Paths { get; set; }
        public DbSet<Patient> Patients { get; set; }
        public DbSet<Raiting> Raitings { get; set; }
        public DbSet<User>  Users { get; set; }

      
        public DbSet<CustomRole> roles { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Appointment>().HasRequired(s => s.Raiting).WithRequiredPrincipal(ad => ad.Appointment);
        }


        public static Context Create()
        {
            return new Context();

        }
    }
}
