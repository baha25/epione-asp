﻿using Data.Infrastructure;
using Domain.Entities;
using ServicePattern;
using System;
using System.Collections.Generic;

namespace Service
{
  public  class ServiceAppointement: Service<Appointment>
    {
        static IDataBaseFactory factory = new DataBaseFactory();
        static IUnitOfWork utk = new UnitOfWork(factory);

        public ServiceAppointement() : base(utk)
        {
        }
      //  Valid , Invalid , Cancelied
        public IEnumerable<Appointment> GetProductByEtatvalide()
        {
            return null;
            return GetMany(m => m.state.Equals("Valide"));
        }
        public IEnumerable<Appointment> GetProductByEtatInvalide()
        {
            return null;
            return GetMany(m => m.state.Equals("Invalide"));
        }
        public IEnumerable<Appointment> GetProductByEtatCancelied()
        {
            return null;
            return GetMany(m => m.state.Equals("Cancelied"));
        }
    }
}
