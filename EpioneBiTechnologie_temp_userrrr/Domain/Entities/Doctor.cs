﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class Doctor:User
    {
   
        public virtual ICollection<Intervention> listeIntervention { get; set; }
        public virtual ICollection<Availability> listeAvailability { get; set; }
        public virtual ICollection<Appointment> Appointment { get; set; }
        public virtual ICollection<Chat> Chat { get; set; }


    }
}
