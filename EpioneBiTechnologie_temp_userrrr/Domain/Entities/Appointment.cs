﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{ //public enum State { Valid , Invalid , Cancelied }
   public  class Appointment
    {
    

        [Key]
        public int appointmentId { get; set; }
        [Required]
        public DateTime dateAppointment { get; set; }
        [Required]
        public string reason { get; set; }
        public string state { get; set; }
        public string message { get; set; }
        public float price { get; set; }

        public virtual Raiting Raiting { get; set; }
        public virtual Path path { get; set; }

        public   virtual Patient Patient { get; set; }
        public virtual Doctor Doctor { get; set; }
      
    }
}
