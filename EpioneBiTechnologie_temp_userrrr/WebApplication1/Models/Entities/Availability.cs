﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public enum Day { Lundi , Mardi , MErcredi , Jeudi , Vendredi , Samedi , Dimanche }
    public class Availability
    {   [Key]
        public int id { get; set; }
        [Required]
        public int startHour { get; set; }
        [Required]
        public int endHour { get; set; }
        [Required]
        public Day Day { get; set; }
         public Doctor doctor { get; set; }
    }
}
