﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class Doctor:User
    {
        [Required]
    
        public virtual ICollection<Intervention> listeIntervention { get; set; }
        public virtual ICollection<Availability> listeAvailability { get; set; }
        public virtual ICollection<Appointment> Appointment { get; set; }
        public virtual ICollection<Chat> Chat { get; set; }


    }
}
