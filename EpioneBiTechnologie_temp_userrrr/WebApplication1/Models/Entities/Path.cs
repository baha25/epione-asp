﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebApplication1.Models
{
    public class Path 
    {   [Key]
        public int pathId { get; set; }
        public string comment { get; set; }
        public virtual ICollection<Appointment> listeAppointment { get; set; }

    }
}
