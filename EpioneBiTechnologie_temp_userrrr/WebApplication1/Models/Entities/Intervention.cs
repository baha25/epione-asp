﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class Intervention
    {
        [Key]
        public int interventionId { get; set; }
        [Required]
        public string Description { get; set; }
        public Doctor doctor { get; set; }
    }
}
