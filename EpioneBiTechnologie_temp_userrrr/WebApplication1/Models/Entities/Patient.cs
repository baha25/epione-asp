﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class Patient:User 
    {
       
        public virtual ICollection<Appointment> Appointment { get; set; }
        public virtual ICollection<Chat> Chat { get; set; }

    }
}
